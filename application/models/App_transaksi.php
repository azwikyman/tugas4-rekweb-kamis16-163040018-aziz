<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class App_transaksi extends CI_Model {

    protected $table = 'transaksi'; //nama tabel dari database
    protected $column_order = array(null, 'nama_tr','jumlah_tr','harga_tr'); //field yang ada di table user
    protected $column_search = array('nama_tr','jumlah_tr','harga_tr'); //field yang diizin untuk pencarian 
    protected $order = array('nama_tr' => 'asc'); // default order 

    function __construct() {
        parent::__construct();
    }

    private function _get_datatables_query() {
        $this->db->from($this->table);
        $i = 0;

        foreach($this->column_search as $item) { // looping awal 
            if($_POST['search']['value']) {
            // jika datatable mengirimkan pencarian dengan metode POST
                if($i===0) {
                // looping awal
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if(isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function insert($table='', $data='') {
        return $this->db->insert($table, $data);
    }

    function get_all($table) {
        $this->db->from($table);
        return $this->db->get();
    }

    function delete($id) {
        $this->db->where('id_tr', $id);
        $this->db->delete($this->table);
        return $this->db->affected_rows();
    }

    function get_where($table = null, $where = null) {
        $this->db->from($table);
        $this->db->where($where);
        return $this->db->get();
    }

    function update($table = null, $data = null, $where = null) {
        $this->db->update($table, $data, $where);
    }
}
 ?>
 
