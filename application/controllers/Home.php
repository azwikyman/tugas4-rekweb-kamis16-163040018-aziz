<?php 
class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('app_user');
        $this->API = 'http://localhost/tugas3-rekweb-kamis16-163040018-aziz';
    }

    public function index() {
        // $data['data'] = $this->app_user->get_where('barang', ['status_item' => 1 ]);
        $status = "1";
        $data['data'] = json_decode($this->curl->simple_get($this->API . '/barang/', array("status_item" => $status)));
        $this->template->fend('index/index', 'index/home', $data);
    }

    public function detail() {
        if(is_numeric($this->uri->segment(3))) {
            $id = $this->uri->segment(3);
            var_dump($id);die;
            $items = json_decode($this->curl->simple_get($this->API . '/barang/', array("id_item" => $id)));
        foreach ($items as $key) {
            $data['nama_item'] = $key->nama_item;
            $data['harga_item'] = $key->harga_item;
            $data['jumlah_item'] = $key->jumlah_item;
            $data['berat_item'] = $key->berat_item;
            $data['status_item'] = $key->status_item;
            $data['gambar_item'] = $key->gambar_item;
            $data['desk_item'] = $key->desk_item;
            $data['id_item'] = $id;

        }
            $this->template->fend('index/index', 'index/item_detail', $data);
        } else {
            redirect('home');
        }
    }

    public function registrasi() {

        if($this->input->post('submit', TRUE) == 'submit') {
            $this->form_validation->set_rules('nama1', 'Nama Depan', "required|min_length[3]");
            $this->form_validation->set_rules('nama2', 'Nama Belakang', "required");
            $this->form_validation->set_rules('user', 'Username', "required");
            $this->form_validation->set_rules('email', 'Email', "required|valid_email");
            $this->form_validation->set_rules('pass1', 'Password', "required|min_length[5]");
            $this->form_validation->set_rules('pass2', 'Ketik Ulang Password', "required|matches[pass1]");
            $this->form_validation->set_rules('jk', 'Jenis Kelamin', "required");
            $this->form_validation->set_rules('telepon', 'Telepon', "required|min_length[8]|numeric");
            $this->form_validation->set_rules('alamat', 'Alamat', "required|min_length[10]");

            if($this->form_validation->run() == TRUE) {
                $data = array(
                    'user' => $this->input->post('user', TRUE),
                    'nama1' => $this->input->post('nama1', TRUE),
                    'nama2' => $this->input->post('nama2', TRUE),
                    'email' => $this->input->post('email', TRUE),
                    'pass1' => password_hash($this->input->post('pass1', TRUE), PASSWORD_DEFAULT, ['cost' => 10]),
                    'jk' => $this->input->post('jk', TRUE),
                    'telepon' => $this->input->post('telepon', TRUE),
                    'alamat' => $this->input->post('alamat', TRUE),
                    'status' => 1
                );
                $insert = $this->curl->simple_post($this->API . '/user/', $data, array(CURLOPT_BUFFERSIZE => 10));
                if($insert) {
                    $this->session->set_flashdata('login', 'User telah dibuat, silahkan login');
                    redirect('home','refresh');
                }
            }
        }

        if($this->session->userdata('user_login') == TRUE) {
            redirect('home');
        }

        $data = array(
            'user' => $this->input->post('user', TRUE),
            'nama1' => $this->input->post('nama1', TRUE),
            'nama2' => $this->input->post('nama2', TRUE),
            'email' => $this->input->post('email', TRUE),
            'jk' => $this->input->post('jk', TRUE),
            'telepon' => $this->input->post('telepon', TRUE),
            'alamat' => $this->input->post('alamat', TRUE),
        );
        $this->session->set_flashdata('gagal', 'Username / email sudah terambil, silahkan coba lagi');
        $this->template->fend('index/index', 'index/register', $data);
    }

    public function login() {
        if($this->input->post('submit') == 'submit') {
            $user = $this->input->post('username', TRUE);
            $pass = $this->input->post('password', TRUE);

            $cek = $this->db->get_where('user', "username = '$user' || email_user = '$user'");

            if($cek->num_rows() > 0) {
                $data = $cek->row();

                if(password_verify($pass, $data->password_user)) {
                    $datauser = array(
                        'user_id' => $data->id_user,
                        'name' => $data->nama_lengkap,
                        'user_login' => TRUE
                    );

                    $this->session->set_userdata($datauser);
                    redirect('home');
                } else {
                    $this->session->set_flashdata('alert', 'Password yang Anda masukkan salah');
                }
            } else {
                $this->session->set_flashdata('alert', 'Username ditolak');
            }
        }
        if($this->session->userdata('user_login') == TRUE) {
            redirect('home');
        }
        $this->load->view('index/login');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('home');
    }
}


 ?>
 
