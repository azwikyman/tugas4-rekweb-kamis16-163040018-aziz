<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        if($this->session->userdata('level') != 1) {
            redirect('login');
        }
        $this->load->model('app_admin');
    }

    public function index() {
        $this->template->admin('admin/dashboard', 'admin/home');
	}

    function get_data_user() {
        $list = $this->app_admin->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama_item;
            $row[] = 'Rp ' . number_format($field->harga_item,0,',','.');
            $row[] = $field->jumlah_item . ' buah';
            $row[] = $field->berat_item . ' kg';
            if($field->status_item == 1) {
                $row[] = '<label class="label-success" style="color:white; padding: 3px 5px;">Aktif</label>';
            } else {
                $row[] = '<label class="label-danger" style="color:white; padding: 3px 5px;">Tidak aktif</label>';
            }
            $row[] = '<a href="'.site_url('item/delete/' . $field->id_item ).'" class="btn btn-danger" onclick="return confirm(\'Yakin akan menghapus?\')"><i class="fa fa-trash"></i></a><a href="'.site_url('item/update_item/' . $field->id_item ).'" class="btn btn-warning"><i class="fa fa-edit"></i></a><a href="'.site_url('item/detail/' . $field->id_item ).'" class="btn btn-success"><i class="fa fa-search-plus"></i></a>';
           
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->app_admin->count_all(),
            "recordsFiltered" => $this->app_admin->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
   
}
