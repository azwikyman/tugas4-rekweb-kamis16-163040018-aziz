<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('app_admin');
    }

    public function index() {
        if($this->input->post('submit') == 'submit') {
            $user = $this->input->post('username', TRUE);
            $pass = $this->input->post('password', TRUE);

            $cek = $this->db->get_where('admin', array('username_admin' => $user));

            if($cek->num_rows() > 0) {
                $data = $cek->row();

                if(password_verify($pass, $data->password_admin)) {
                    $datauser = array(
                        'user' => $data->fullname_admin,
                        'level' => $data->level_admin,
                        'login' => TRUE
                    );

                    $this->session->set_userdata($datauser);
                    redirect('admin');
                } else {
                    $this->session->set_flashdata('alert', 'Password yang Anda masukkan salah');
                }
            } else {
                $this->session->set_flashdata('alert', 'Username ditolak');
            }
        }
        if($this->session->userdata('login') == TRUE) {
            redirect('admin');
        }
        $this->load->view('admin/login_form');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
}
