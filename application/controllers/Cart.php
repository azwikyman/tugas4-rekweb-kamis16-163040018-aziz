<?php 
class Cart extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('app_user');
        $this->API = 'http://localhost/tugas3-rekweb-kamis16-163040018-aziz';
    }

    public function index() {
        $this->template->fend('index/index', 'index/cart');
    }

    public function add() {
       if($this->session->userdata('user_login') == TRUE) {
            if(is_numeric($this->uri->segment(3))) {
                $id = $this->uri->segment(3);
                $get = json_decode($this->curl->simple_get($this->API . '/barang/', array("id_item" => $id)));
                if($get[0]->jumlah_item == 0) {
                    $this->session->set_flashdata('kosong', 'Mohon maaf, stok produk ini habis, silahkan tunggu beberapa hari');
                    redirect('home');
                } else {
                    $data = array(
                        'id' =>$get[0]->id_item,
                        'name' => $get[0]->nama_item,
                        'price' => $get[0]->harga_item,
                        'weight' => $get[0]->berat_item,
                        'qty' => 1
                    );
                    $this->cart->insert($data);
                    redirect('home');
                }
            } else {
                redirect('home');
            }
        } else {
            $this->session->set_flashdata('alert', 'Anda harus login terlebih dahulu!');
            redirect('home');
        } 
    }

    public function update() {
        if($this->uri->segment(3)) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('qty', 'Jumlah Pesananan', 'required|numeric');
            if($this->form_validation->run() == TRUE) {
                $data = array(
                    'rowid' => $this->uri->segment(3),
                    'qty'   => $this->input->post('qty', TRUE)
                );
                
                $this->cart->update($data);
                redirect('cart');
            } else {
                $this->template->fend('index/index', 'index/cart');
            }
        } else {
            redirect('cart');
        }
    }

    public function delete() {
        if($this->uri->segment(3)) {
            $rowid = $this->uri->segment(3);
            $this->cart->remove($rowid);

            redirect('cart');    
        } else {
            redirect('cart');
        }
    }

    public function cekout() {
        if($this->cart->contents() == null) {
            $this->session->set_flashdata('cart', 'Cart kosong, silahkan isi terlebih dahulu');
            redirect('home');
        } else {
            foreach ($this->cart->contents() as $key) {
                $data = array(
                    'nama_tr' => $key['name'],
                    'jumlah_tr' => $key['qty'],
                    'harga_tr' => $key['price'] * $key['qty']        
                );

                $stok = json_decode($this->curl->simple_get($this->API . '/barang/', array("id_item" => $key['id'])));
                $stok1 = $stok[0]->jumlah_item;
                $hasil = $stok1 - $key['qty'];
                $data1 = array(
                    'id_item' => $key['id'],
                    'jumlah_item' => $hasil,
                    'nama_item' => $stok[0]->nama_item,
                    'harga_item' => $stok[0]->harga_item,
                    'berat_item' => $stok[0]->berat_item,
                    'status_item' => $stok[0]->status_item,
                    'gambar_item' => $stok[0]->gambar_item,
                    'desk_item' => $stok[0]->desk_item
                );
                $this->curl->simple_put($this->API . '/barang/', $data1, array(CURLOPT_BUFFERSIZE => 10));

                $data2 = array(
                    'nama_tr' => $key['name'],
                    'jumlah_tr' => $key['qty'],
                    'harga_tr' => $key['price'] * $key['qty']
                );
                $this->curl->simple_post($this->API . '/transaksi/', $data2, array(CURLOPT_BUFFERSIZE => 10));
            };
            $this->cart->destroy();
            $this->session->set_flashdata('sukses', 'Pesanan anda sudah diproses, terima kasih.');
            redirect('home');
        }   
    }

    public function hapuscart() {
        $this->cart->destroy();
        redirect('cart');
    }
}

?>
