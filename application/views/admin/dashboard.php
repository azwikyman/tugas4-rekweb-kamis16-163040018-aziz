<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Halaman Admin</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>admin_asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo base_url(); ?>admin_asset/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Data tables -->
        <link href="<?php echo base_url(); ?>admin_asset/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>admin_asset/css/responsive.bootstrap.min.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="<?php echo base_url(); ?>admin_asset/css/custom.min.css" rel="stylesheet">
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <?php $this->load->view('admin/nav'); ?>
                <!-- page content -->
                <div class="right_col" role="main">
                    <?= $contents; ?>
                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>
        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>admin_asset/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url(); ?>admin_asset/js/bootstrap.min.js"></script>
        <!-- Data Tables -->
        <script src="<?php echo base_url(); ?>admin_asset/js/jquery.dataTables.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="<?php echo base_url(); ?>admin_asset/js/custom.min.js"></script>
        <script>
        var table;
        $(document).ready(function() {
        //datatables
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [],
            
                "ajax": {
                    "url": "<?php echo site_url('admin/get_data_user')?>",
                    "type": "POST"
                },
        
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "orderable": false,
                    },
                ],
            });
        });
        $('.alert-message').alert().delay(3000).slideUp('slow');
        </script>
    </body>
</html>
