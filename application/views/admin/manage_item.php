<div class="x_panel">
    <div class="x_title">
        <h2>Kelola Item</h2>
        <div style="float:right">
            <a href="<?php echo base_url();?>item/add_item" class="btn btn-primary">Tambah Item</a>
        </div>
        <div class="clearfix"></div>
        <?php echo validation_errors('<p style="color:red">', '</p>') ?>
        <?php 
            if($this->session->flashdata('alert')) {
                echo '<div class="alert alert-danger alert-message">';
                echo $this->session->flashdata('alert');
                echo "</div>";
            }
            if($this->session->flashdata('upload')) {
                echo '<div class="alert alert-info alert-message">';
                echo $this->session->flashdata('upload');
                echo "</div>";
            }
            if($this->session->flashdata('insert')) {
                echo '<div class="alert alert-info alert-message">';
                echo $this->session->flashdata('insert');
                echo "</div>";
            }
         ?>
    </div>

    <div class="x_content">
        <table class="table table-striped table-bordered dt-responsive nowrap" id='table'>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Item</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Berat</th>
                    <th>Status</th>
                    <th>Opsi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Nama Item</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Berat</th>
                    <th>Status</th>
                    <th>Opsi</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
