<div class="x_panel">
	<div class="x_title">
		<h2><?= $header ?></h2>
		<div class="clearfix"></div>
		<?php echo validation_errors('<p style="color:red">', '</p>') ?>
		<?php
			if($this->session->flashdata('alert')) {
				echo '<div class="alert alert-danger alert-message">';
					echo $this->session->flashdata('alert');
				echo "</div>";
			}
		?>
	</div>
	
	<div class="x_content">
		<br />
		<form action="" enctype="multipart/form-data" method="post" class="form-horizontal form-label-left">
			
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Nama Item</label>
				<div class="col-md-7 col-sm-6 col-xs-12">
					<input type="text" class="form-control col-md-7 col-xs-12" name="nama" value="<?php echo $nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Gambar Item</label>
				<div class="col-md-7 col-sm-6 col-xs-12">
					<?php 
						if(isset($gambar)) {
							echo '<input type="hidden" name="old_pict" value="'.$gambar.'">';
							echo '<img src="'.base_url() . 'asset/upload/'. $gambar. '" width="30%">';
						}
					 ?>
					 <div class="clear-fix"></div>
					 <br>
					<input type="file" class="form-control col-md-7 col-xs-12" name="foto">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Harga Item</label>
				<div class="col-md-7 col-sm-6">
					<div class="input-group">
						<span class="input-group-addon">Rp</span>
						<input type="number" class="form-control col-md-7 col-xs-12" name="harga" value="<?php echo $harga; ?>">
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Berat Item</label>
				<div class="col-md-7 col-sm-6">
					<div class="input-group">
						<input type="number" class="form-control col-md-7" name="berat" value="<?php echo $berat; ?>">
						<span class="input-group-addon">Kg</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Jumlah Stok</label>
				<div class="col-md-7 col-sm-6">
					<div class="input-group">
						<input type="number" class="form-control col-md-7" name="jumlah" value="<?php echo $jumlah; ?>">
						<span class="input-group-addon">Buah</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Status</label>
				<div class="col-md-7 col-sm-6">
					<select name="status" class="form-control">
						<option value="">--Pilih Status--</option>
						<option value="1" <?php if($status == 1) {echo "selected";} ?>>Aktif</option>
						<option value="2" <?php if($status == 2) {echo "selected";} ?>>Non-Aktif</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Deskripsi</label>
				<div class="col-md-7 col-sm-1">
					<textarea class="form-control" rows="3" name="desk" ><?php echo $desk; ?></textarea>
				</div>
			</div>
			<div class="ln-solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="submit" class="btn btn-success" name="submit" value="submit">Submit</button>
					<button type="button" onclick="window.history.go(-1)" class="btn btn-warning">Kembali</button>
				</div>
			</div>
		</form>
	</div>
</div>
