<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Motekar Movies - Admin</title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>admin_asset/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>admin_asset/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <style>
        body {
            background: #004049;
        }
        .well {
            border-radius: 0px;
            margin-top: 10%;paint-order:
            color: #616161;
        }
        .well hr {
            margin: 5px;
            color: #f7f7f7;
        }
        .header {
            font-size: 40px;
            color: #f7f7f7;
        }
        .header .fa {
            border: 2px solid #fcfcfc;
            border-radius: 50%;
            padding: 5px;
        }
        .container {
            padding-top: 5%;
        }
        .form-control {
            padding: 20px 10px;
            font-size: 20px;
        }
        .btn {
            padding: 5px 20px;
            font-size: 16px;
            border-radius: 0px;
        }
    </style>
</head>
<body>
    <div class="container">
        <center>
            <span class="header"><i class="fa fa-shopping-cart"></i>Motekar Movies - Admin</span>
        </center>
        <div class="col-md-4 col-sm-12 col-md-offset-4">
            <?php 

                if($this->session->flashdata('alert')) {
                    echo '<div class="alert alert-warning alert-message">';
                    echo $this->session->flashdata('alert');
                    echo '</div>';
                }

             ?>
            <form action="" method="post" class="well">
                <h3><i class="fa fa-user"></i>Silahkan Login</h3>
                <hr />
                <br>

                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" placeholder="Username" name="username">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" placeholder="Password" name="password">
                </div>

                <div class="form-group" style="text-align: right">
                    <button type="submit" class="btn btn-primary" name="submit" value="submit">Sign In</button>
                </div>
            </form>
        </div>
    </div>
    <!-- JQuery -->
    <script src="<?php echo base_url(); ?>admin_asset/js/jquery.min.js"></script>
    <!-- Boostrap -->
    <script src="<?php echo base_url(); ?>admin_asset/js/bootstrap.min.js"></script>
    <script>
        $('.alert-message').alert().delay(3000).slideUp('slow');
    </script>
</body>
</html>
