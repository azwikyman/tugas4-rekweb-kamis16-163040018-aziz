<center><h4><i class="fa fa-shopping-cart"></i> Daftar Barang</h4></center>
<hr>
<br>
<?php echo validation_errors('<p style="color:red">','</p>'); ?>
<table>
    <div class="row">
        <div class="col-md-10 col-sm-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Nama barang</td>
                        <td>Berat</td>
                        <td>Jumlah</td>
                        <td>Harga Total</td>
                        <td width="200px">Opsi</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($this->cart->contents() as $key) :?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $key['name']; ?></td>
                        <td><?php echo $key['weight'];?> Kg</td>
                        <td><?php echo $key['qty']; ?> Buah</td>
                        <td>Rp <?php echo number_format(($key['qty'] * $key['price']), 0, ',','.');?></td>
                        <td>
                            <a href="#<?php echo $key['rowid']; ?>" class="btn-floating orange"><i class="fa fa-edit"></i></a>
                            <a href="<?php echo base_url() ?>/cart/delete/<?php echo $key['rowid']; ?>" class="btn-floating red" onclick="return confirm('Anda yakin akan hapus item?')"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                    </tr>
                    <div class="modal" id="<?php echo $key['rowid']; ?>">
                        <form action="<?php echo base_url(); ?>cart/update/<?php echo $key['rowid']; ?>" method="post">
                            <div class="row">
                                <div class="col m10 s12 offset-m1">
                                    <div class="modal-content">
                                        <h5><i class="fa fa-edit"></i> Edit Pesanan</h5>
                                        <div class="input-field">
                                            <input type="text" name="qty" value="<?php echo $key['name']; ?>" id="qty<?php echo $key['rowid']; ?>" readonly="readonly">
                                            <label for="name<?php echo $key['rowid']; ?>">Nama Barang:</label>
                                        </div>
                                        <div class="input-field">
                                            <input type="number" name="qty" value="<?php echo $key['qty']; ?>" id="qty<?php echo $key['rowid']; ?>" autofocus>
                                            <p style="color: #6b6b6b; margin-top: -15px"><i>*isi dengan angka</i></p>
                                            <label for="qty<?php echo $key['rowid']; ?>">Jumlah Pesanan:</label>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" name="submit" value="submit" class="modal-action btn blue">Update Pesanan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="4">Total</td>
                        <td colspan="2">Rp <?php echo number_format($this->cart->total(), 0, ',','.');?></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <button type="button" class="btn blue" onclick="window.history.go(-1)">Kembali</button>
            <a href="<?php echo base_url(); ?>cart/cekout" class="btn green">cekout</a>
            <a href="<?php echo base_url(); ?>cart/hapuscart" class="btn red" onclick="return confirm('Anda yakin akan hapus isi cart?')">Hapus isi Cart</a>
        </div>
    </div>
</table>
